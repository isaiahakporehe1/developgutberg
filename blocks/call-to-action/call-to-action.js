(function (blocks, editor, element) {
    var el = element.createElement;

    blocks.registerBlockType('mcb/call-to-action', {
        title: 'MCB: Call to Action', // The title of block in editor.
        icon: 'admin-comments', // The icon of block in editor.
        category: 'common', // The category of block in editor.
            //some bit of attrbutes to spice up
            attributes: {
                content: {
                    type: 'string',
                    default: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco '
                },
                button: {
                    type: 'string',
                    default: 'Join Today'
                }
            },
            
     
        edit: function (props) {
            return (
                el('div', { className: props.className },
                    el(
                        editor.RichText,
                        {
                            tagName: 'div',
                            className: 'mcb-call-to-action-content',
                            value: props.attributes.content,
                            onChange: function (content) {
                                props.setAttributes({ content: content });
                            }
                        }
                    ),
                    el(
                        editor.RichText,
                        {
                            tagName: 'span',
                            className: 'mcb-call-to-action-button',
                            value: props.attributes.button,
                            onChange: function (content) {
                                props.setAttributes({ button: content });
                            }
                        }
                    ),
                )
            );
        },


        save: function (props) {
            return (
                el('div', { className: props.className },
                    el(editor.RichText.Content, {
                        tagName: 'p',
                        className: 'mcb-call-to-action-content',
                        value: props.attributes.content,
                    }),
                    el('button', { className: 'mcb-call-to-action-button' },
                        props.attributes.button
                    )
                )
            );
        },
        
    });
})(window.wp.blocks, window.wp.editor, window.wp.element);